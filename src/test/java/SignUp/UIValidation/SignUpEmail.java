package SignUp.UIValidation;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import Basic.Base;
import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidElement;

public class SignUpEmail extends Base{

	@Test
	public void SignUpFlow() throws InterruptedException, IOException, ParseException {
		
		
		
		log.debug("--------------Login to Continue Page Validations--------------");
		assertElementPresent(loadElement("HomePage","appointments"));
		clickElement(loadElement("HomePage","appointments"));
		
		assertElementPresent(loadElement("LoginToContinue","signUpButton"));
		clickElement(loadElement("LoginToContinue","signUpButton"));

		
		
		log.debug("--------------Sign Up Page Validations--------------");
				
		
		softAssertTextOfElement(loadElement("SignUpEmail","signUpLabel"),getUserData("signUp","headerSignUp"));
		softAssertTextOfElement(loadElement("SignUpEmail","enterEmailAddressLabel"),getUserData("signUp","enterEmailText"));
		
		assertElementPresent(loadElement("SignUpEmail","nextButton"));
		assertElementPresent(loadElement("SignUpEmail","emailInput"));


		
		log.debug("--------------ENTERING Email Value--------------");
		
		sendKeysElement(loadElement("SignUpEmail","emailInput"),getUserData("signUp","emailSignUp2")+random+getUserData("signUp","domainName"));
		clickElement(loadElement("SignUpEmail","nextButton"));		
		
		clickElement(loadElement("LoginPage","permissionForId"));
		clickElement(loadElement("LoginPage","permissionForCalls"));
		
		clickElement(loadElement("SignUpEmail","nextButton"));


		Thread.sleep(3000);
		
		
		log.info("--------------Let's Verify OTP Page-----------------");
		
		softAssertTextOfElement(loadElement("OTPVerifyPage","letsVerifyLabel"),getUserData("signUp","verifyLabel"));
		softAssertTextOfElement(loadElement("OTPVerifyPage","enterOtpLabel"),getUserData("signUp","enterOTPLabel"));
		softAssertTextOfElement(loadElement("OTPVerifyPage","verifyTheOTPSentOnLabel"),getUserData("signUp","verifyOTPSentLabel"));
		softAssertTextOfElement(loadElement("OTPVerifyPage","emailSendOnIdValue"),getUserData("signUp","emailSignUp2")+random+getUserData("signUp","domainName"));
		softAssertTextOfElement(loadElement("OTPVerifyPage","resendOTPButton"),getUserData("signUp","resendOTPButton"));
		
		assertElementPresent(loadElement("OTPVerifyPage","nextButton"));

		
		String otp =getOtpFromDBUsingEmailId(getUserData("signUp","emailSignUp2")+random+getUserData("signUp","domainName"));
		System.out.println(otp);
	
		
		
		
		sendKeysElement(loadElement("OTPVerifyPage","inputOTP1"),String.valueOf(otp.charAt(2)));
		sendKeysElement(loadElement("OTPVerifyPage","inputOTP2"),String.valueOf(otp.charAt(3)));
		sendKeysElement(loadElement("OTPVerifyPage","inputOTP3"),String.valueOf(otp.charAt(4)));
		sendKeysElement(loadElement("OTPVerifyPage","inputOTP4"),String.valueOf(otp.charAt(5)));
		
		
		clickElement(loadElement("OTPVerifyPage","nextButton"));		
		Thread.sleep(6000);

		//////////////////////////////Personal Details
		softAssertTextOfElement(loadElement("PersonalDetails","personalDetailsLabel"),getUserData("signUp","personalDetailsLabel"));

		assertElementPresent(loadElement("PersonalDetails","firstNameInput"));
		assertElementPresent(loadElement("PersonalDetails","lastNameInput"));
		assertElementPresent(loadElement("PersonalDetails","dateOfBirth"));
		assertElementPresent(loadElement("PersonalDetails","gender"));
		assertElementPresent(loadElement("PersonalDetails","mobileNumber"));
		assertElementPresent(loadElement("PersonalDetails","nextButton"));
		
		

		sendKeysElement(loadElement("PersonalDetails","firstNameInput"),getUserData("signUp","firstName"));
		sendKeysElement(loadElement("PersonalDetails","lastNameInput"),getUserData("signUp","lastName"));
		sendKeysElement(loadElement("PersonalDetails","gender"),getUserData("signUp","gender"));
		sendKeysElement(loadElement("PersonalDetails","mobileNumber"),getUserData("signUp","mobileNumber")+random);

		
		clickElement(loadElement("PersonalDetails","dateOfBirth"));
		clickElement(loadElement("PersonalDetails","dateOfBirthOK"));

	//	clickElement(loadElement("HomePage","appointments").personalDetails.countryCode);
	//	clickElement(loadElement("HomePage","appointments").personalDetails.countryCodeIndia);
		
		

		
		clickElement(loadElement("PersonalDetails","nextButton"));
		
		
//////////////////////	Address Details	
		softAssertTextOfElement(loadElement("AddressDetails","yourAddressDetailsLabel"),getUserData("signUp","addressDetailsLabel"));

		assertElementPresent(loadElement("AddressDetails","houseLabel"));
		assertElementPresent(loadElement("AddressDetails","zipCode"));

		sendKeysElement(loadElement("AddressDetails","houseLabel"),getUserData("signUp","houseNumber"));
		sendKeysElement(loadElement("AddressDetails","zipCode"),getUserData("signUp","zipCode"));
		
		
	
		Thread.sleep(3000);

		clickElement(loadElement("AddressDetails","nextButton"));

		/////////////////////////////Create Password Page
		softAssertTextOfElement(loadElement("CreatePassword","passwordLabel"),getUserData("signUp","yourPassword"));

		assertElementPresent(loadElement("CreatePassword","passwordInput"));
		assertElementPresent(loadElement("CreatePassword","confirmPasswordInput"));
		assertElementPresent(loadElement("CreatePassword","nextButton"));
		
		sendKeysElement(loadElement("CreatePassword","passwordInput"),getUserData("signUp","patient1","password"));
		sendKeysElement(loadElement("CreatePassword","confirmPasswordInput"),getUserData("signUp","patient1","confirmPassword"));

		clickElement(loadElement("AddressDetails","nextButton"));

		
		
		Thread.sleep(3000);
		softClickElement(loadElement("SetQuickAccessKey","setLater"));

		Thread.sleep(3000);
		assertElementPresent(loadElement("HomePage","appointments"));

		softAssert.assertAll();
		
		
		
		
		
		
		 
		
		


		
	}
}
