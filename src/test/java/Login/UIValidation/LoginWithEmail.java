package Login.UIValidation;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
//import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.mongodb.client.model.geojson.Point;

import java.time.Duration;





import Basic.Base;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class LoginWithEmail extends Base  {
	
	@Test
	public void LoginSuccessWithEmail() throws InterruptedException, IOException, ParseException {
		
		//getUserData("login","email5")
		
		log.debug("--------------Login to Continue Page Validations--------------");
		assertElementPresent(loadElement("HomePage","appointments"));
		clickElement(loadElement("HomePage","appointments"));
		
		
		assertElementPresent(loadElement("LoginToContinue","loginButton"));
		clickElement(loadElement("LoginToContinue","loginButton"));

		
		
		log.debug("--------------Login Page Validations--------------");
		
		
		checkAttributeValue(loadElement("LoginPage","loginByEmailButton"),"checked","false");
		
		
		//String s =getUserData("login","randomText");
		//System.out.println(s);
		softAssertTextOfElement(loadElement("LoginPage","loginByUserIdButton"),getUserData("login","userIdText"));
		softAssertTextOfElement(loadElement("LoginPage","loginByEmailButton"),getUserData("login","labelEmailRadioBtn"));
		softAssertTextOfElement(loadElement("LoginPage","loginByMobileNumberButton"),getUserData("login","labelMobileNumberRadioBtn"));
		
		clickElement(loadElement("LoginPage","loginByEmailButton"));
		checkAttributeValue(loadElement("LoginPage","loginByEmailButton"),"checked","true");

		
		
		assertElementPresent(loadElement("LoginPage","signUpButton"));
		assertElementPresent(loadElement("LoginPage","loginButton"));
		
		softAssertElementPresent(loadElement("LoginPage","mphRxLogo"));
		
		softAssertTextOfElement(loadElement("LoginPage","forgotPassword"),getUserData("login","forgotPasswordText"));
		assertElementPresent(loadElement("LoginPage","inputEmail"));
		assertElementPresent(loadElement("LoginPage","password"));
		
		
		log.debug("--------------ENTERING Login Values--------------");
		
		//sendKeysElement(minerva.loginPage.inputEmail,getUserData("login","email5").logindata.email);
		sendKeysElement(loadElement("LoginPage","inputEmail"),getUserData("login","email5"));

		sendKeysElement(loadElement("LoginPage","password"),getUserData("login","password5"));
		clickElement(loadElement("LoginPage","loginButton"));		
		clickElement(loadElement("LoginPage","permissionForCalls"));
		
		
		
		softClickElement(loadElement("SetQuickAccessKey","setLater"));
		assertElementPresent(loadElement("HomePage","appointments"));

		softAssert.assertAll();
		
		


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
		
	
	}

}
