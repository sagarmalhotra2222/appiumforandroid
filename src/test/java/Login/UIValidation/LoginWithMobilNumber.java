package Login.UIValidation;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
//import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.mongodb.client.model.geojson.Point;

import java.time.Duration;





import Basic.Base;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class LoginWithMobilNumber extends Base  {
	
	@Test
	public void LoginSuccessWithMobileNumber() throws InterruptedException, IOException, ParseException {
		
		
		
		log.debug("--------------Login to Continue Page Validations--------------");
		assertElementPresent(loadElement("HomePage","appointments"));
		clickElement(loadElement("HomePage","appointments"));
		
		assertElementPresent(loadElement("LoginToContinue","loginButton"));
		clickElement(loadElement("LoginToContinue","loginButton"));

		
		
		log.debug("--------------Login Page Validations--------------");
		
		
		checkAttributeValue(loadElement("LoginPage","loginByMobileNumberButton"),"checked","false");
		
		
		
		softAssertTextOfElement(loadElement("LoginPage","loginByUserIdButton"),getUserData("login","userIdText"));
		softAssertTextOfElement(loadElement("LoginPage","loginByEmailButton"),getUserData("login","labelEmailRadioBtn"));
		softAssertTextOfElement(loadElement("LoginPage","loginByMobileNumberButton"),getUserData("login","labelMobileNumberRadioBtn"));
		
		clickElement(loadElement("LoginPage","loginByMobileNumberButton"));
		checkAttributeValue(loadElement("LoginPage","loginByMobileNumberButton"),"checked","true");

		
		
		assertElementPresent(loadElement("LoginPage","signUpButton"));
		assertElementPresent(loadElement("LoginPage","loginButton"));
		
		softAssertElementPresent(loadElement("LoginPage","mphRxLogo"));
		
		softAssertTextOfElement(loadElement("LoginPage","forgotPassword"),getUserData("login","forgotPasswordText"));
		
		
		assertElementPresent(loadElement("LoginPage","inputMobileNumber"));
		assertElementPresent(loadElement("LoginPage","password"));
		
		
		log.debug("--------------ENTERING Login Values--------------");
		
		clickElement(loadElement("LoginPage","countryCode"));
		clickElement(loadElement("LoginPage","indiaCountryCode"));
		

		
		sendKeysElement(loadElement("LoginPage","inputMobileNumber"),getUserData("login","mobile5"));

		sendKeysElement(loadElement("LoginPage","password"),getUserData("login","password5"));
		clickElement(loadElement("LoginPage","loginButton"));		
		clickElement(loadElement("LoginPage","permissionForCalls"));
		
		
		
		softClickElement(loadElement("SetQuickAccessKey","setLater"));
		assertElementPresent(loadElement("HomePage","appointments"));

		softAssert.assertAll();
		
		


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
		
	
	}

}
