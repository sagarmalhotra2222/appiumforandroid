package Login.UIValidation;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.io.IOException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
//import org.openqa.selenium.interactions.Coordinates;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import com.mongodb.client.model.geojson.Point;

import java.time.Duration;





import Basic.Base;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;


public class LoginWithId extends Base  {
	
	@Test
	public void LoginSuccessWithId() throws InterruptedException, IOException, ParseException {
		
		
		
		log.debug("--------------Login to Continue Page Validations--------------");
		assertElementPresent(loadElement("HomePage","appointments"));
		clickElement(loadElement("HomePage","appointments"));
		
		softAssertTextOfElement(loadElement("LoginToContinue","ifYouAreANewUserText"),getUserData("login","newUserSignUpText"));
		softAssertTextOfElement(loadElement("LoginToContinue","loginToContinueText"),getUserData("login","loginToContinueText"));
		softAssertTextOfElement(loadElement("LoginToContinue","signUpButton"),getUserData("login","signUpText"));

		assertElementPresent(loadElement("LoginToContinue","loginButton"));
		clickElement(loadElement("LoginToContinue","loginButton"));

		
		
		log.debug("--------------Login Page Validations--------------");
		
		checkAttributeValue(loadElement("LoginPage","loginByUserIdButton"),"checked","true");
		
		softAssertTextOfElement(loadElement("LoginPage","loginByUserIdButton"),getUserData("login","userIdText"));
		softAssertTextOfElement(loadElement("LoginPage","loginByEmailButton"),getUserData("login","labelEmailRadioBtn"));
		softAssertTextOfElement(loadElement("LoginPage","loginByMobileNumberButton"),getUserData("login","labelMobileNumberRadioBtn"));
		
		
		
		assertElementPresent(loadElement("LoginPage","signUpButton"));
		assertElementPresent(loadElement("LoginPage","loginButton"));
		
		softAssertElementPresent(loadElement("LoginPage","mphRxLogo"));
		
		softAssertTextOfElement(loadElement("LoginPage","forgotPassword"),getUserData("login","forgotPasswordText"));
		assertElementPresent(loadElement("LoginPage","inputUserId"));
		assertElementPresent(loadElement("LoginPage","password"));
		
		
		log.debug("--------------ENTERING Login Values--------------");
		
		sendKeysElement(loadElement("LoginPage","inputUserId"),getUserData("login","userName"));

		sendKeysElement(loadElement("LoginPage","password"),getUserData("login","password5"));
		clickElement(loadElement("LoginPage","loginButton"));		
		clickElement(loadElement("LoginPage","permissionForCalls"));
		
		
		
		softClickElement(loadElement("SetQuickAccessKey","setLater"));
		assertElementPresent(loadElement("HomePage","appointments"));

		softAssert.assertAll();
		
		


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
		
		
	
	}

}
