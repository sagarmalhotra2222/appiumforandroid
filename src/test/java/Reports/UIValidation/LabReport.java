package Reports.UIValidation;


import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import Basic.Base;

public class LabReport extends Base {

	@Test
	public void LabReportPage() throws IOException, ParseException {
		
		LoginByUserId(getUserData("login","userNameReports") ,getUserData("login","password5") );
		clickElement(loadElement("HomePage","reports"));
		
		
		///////// Validating Results page
		softAssertElementPresent(loadElement("Results","myHealthLabel"));
		softAssertElementPresent(loadElement("Results","vitalsLabel"));
		softAssertElementPresent(loadElement("Results","resultsLabel"));
		softAssertElementPresent(loadElement("Results","documentsLabel"));
		softAssertElementPresent(loadElement("Results","labReportLogo"));
		softAssertElementPresent(loadElement("Results","test1Label"));
		softAssertElementPresent(loadElement("Results","patientLabel"));
		softAssertElementPresent(loadElement("Results","patientValue"));
		softAssertElementPresent(loadElement("Results","orderingPhysicianLabel"));
		softAssertElementPresent(loadElement("Results","orderDateTimeValue"));
		softAssertElementPresent(loadElement("Results","reportStatusLabel"));
		softAssertElementPresent(loadElement("Results","reportStatusValue"));
		softAssertElementPresent(loadElement("Results","orderDateTimeLabel"));
		softAssertElementPresent(loadElement("Results","orderDateTimeValue"));
		softAssertElementPresent(loadElement("Results","hospitalLabel"));
		softAssertElementPresent(loadElement("Results","hospitalValue"));
		softAssertElementPresent(loadElement("Results","viewTestButton"));
		//driver.findElementsByAccessibilityId("");
		
		softAssert.assertAll();

	}
	
}
