package Reports.UIValidation;


import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.testng.annotations.Test;

import Basic.Base;

public class LabReportDetails extends Base {

	@Test
	public void LabReportDetailsPage() throws IOException, ParseException {
		
		LoginByUserId(getUserData("login","userNameReports") ,getUserData("login","password5") );
		clickElement(loadElement("HomePage","reports"));
		
		
		////////////////////Results page
		
		assertElementPresent(loadElement("Results","viewTestButton"));
		clickElement(loadElement("Results","viewTestButton"));
		
		///////////////////Results Details Page
		
		softAssertElementPresent(loadElement("ResultDetails","resultDetailsLabel"));
		softAssertElementPresent(loadElement("ResultDetails","testName"));
		softAssertElementPresent(loadElement("ResultDetails","resultDateLabel"));
		softAssertElementPresent(loadElement("ResultDetails","resultDateValue"));
		softAssertElementPresent(loadElement("ResultDetails","orderingPhysicianLabel"));
		softAssertElementPresent(loadElement("ResultDetails","orderingPhysicianValue"));
		softAssertElementPresent(loadElement("ResultDetails","parameterLabel"));
		softAssertElementPresent(loadElement("ResultDetails","parameterUnit"));
		softAssertElementPresent(loadElement("ResultDetails","parameterValue"));
		softAssertElementPresent(loadElement("ResultDetails","refLabel"));
		softAssertElementPresent(loadElement("ResultDetails","refRange"));
		softAssertElementPresent(loadElement("ResultDetails","resultValue"));
		softAssertElementPresent(loadElement("ResultDetails","viewReportButton"));
		softAssertElementPresent(loadElement("ResultDetails","testDetailsButton"));
		softAssertElementPresent(loadElement("ResultDetails","observationGraph"));

		
		
		assertElementPresent(loadElement("ResultDetails","viewReportButton"));
		clickElement(loadElement("ResultDetails","viewReportButton"));
		
		
		/////////////////////////PDF Report
		
		clickElement(loadElement("ResultDetails","AllowButtonForAccess"));
		assertElementPresent(loadElement("ResultDetails","pdfReportOpen"));

		
		

		
		
		softAssert.assertAll();

	}
	
}
	