package Basic;

import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

//import LogIn.ScrollPageDown;
//import OR.Minerva;
import io.appium.java_client.MobileBy;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
//import io.appium.java_client.android.AndroidKeyCode;
//import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.touch.LongPressOptions;
//import testData.BaseTestData;

public class Base {
	
	public AndroidDriver<AndroidElement> driver;

	public int random;
	public String OR;
	
	public Properties elements;
	public Properties config; 
		
	public TouchAction t; 
	public Logger log;
	public SoftAssert softAssert;
	
	
	public String userName;
	public String database;
	public String password1;
	public String IP;
	
	@BeforeMethod
	public void setup() throws InterruptedException, IOException {
		config = new Properties();
//		AppiumDriverLocalService service =AppiumDriverLocalService.buildDefaultService();
//		System.out.println(service.isRunning());
//		service.start();
//		System.out.println("=================================="+service.isRunning());
	//	Properties elements = new Properties();
		
 		Runtime.getRuntime().exec(System.getProperty("user.dir")+"//src//main//java//resources//appium.sh");
 		Thread.sleep(5000);
		
		 FileInputStream configFile = new FileInputStream(System.getProperty("user.dir")+"//conf.properties");
		 config.load(configFile);
		 
		 //System.out.println(config.getProperty("Device"));
	     log = LogManager.getLogger(this.getClass().getName());

			String device = config.getProperty("Device");
			 userName = config.getProperty("userName");
			 database = config.getProperty("database");
			 password1 = config.getProperty("password1");
			 IP = config.getProperty("IP");
	    
	    if(device.equals("Android"))
	    {
	    	 log.info("Running test case on Android Server");
	    	OR = "AndroidOR";	   
	    	System.out.println("upto setup");
	    	androidSetup();    
	    	}
	    
	    else if(device == "IOS") {
	    	
	    }
	    
	    else {
	    	log.fatal("No Device Value  ---- Please Set Device Value in conf.properties");
	    }
	    
	    
	    
	    
		
	   	}
	
	@AfterMethod
	public void Closing(ITestResult result) throws IOException {
		if(result.isSuccess()) {
			takeScreenShotPassed(this.getClass().getName());
			
		}
		else {
			takeScreenShotFailed(this.getClass().getName());
		}
		
		driver.quit();
		
	}
	
	
	
	
	public void androidSetup() throws InterruptedException, IOException {
		
		//minerva= new Minerva();
//	     baseTestData = new BaseTestData();
	     softAssert= new SoftAssert();
	     
	     log.info("Starting Test Case "+ this.getClass().getName());
	    // log = LogManager.getLogger(Login.class.getName());
		log.info("**************************************"+this.getClass().getName()+"*****************************************");

	     
	     
	     /////////////////////////Config//////////////////////////////
		DesiredCapabilities capabilities = new DesiredCapabilities();
		//capabilities.setCapability("noReset", true);  
	    capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android device");
       capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "uiautomator2");
       capabilities.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir")+"//src//Fisike-global-mphrx.apk");
	     driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	     
	     driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	     
	     ///////////////////////Unlock Device//////////////////////////////////
	    
	     if(isDeviceLocked()) 
	    	 unlockDevice();	     
	     
	     
	     ///////////////////////////Is App Installed Now//////////////////////////////
	     if(isAppFreshlyInstalled()) 
	    	 closeAllIntroTabs();
	     
			int random = RandomNumber();

	     
	   	}
	
	
	
	public String getUserData(String File , String object, String val ) throws FileNotFoundException, IOException, ParseException {
		String remainingPath = "";

		config = new Properties();
		//	Properties elements = new Properties();
			
			 FileInputStream configFile = new FileInputStream(System.getProperty("user.dir")+"//conf.properties");
			 config.load(configFile);
			 
		if(File == "results") {
			log.debug("File == results");
			remainingPath = "reports//results.json";
		}
		
		else if (File == "login") {
			log.debug("File == login");
			remainingPath = "signIn//signIn.json";
		}
		
		else if (File == "signUp") {
			log.debug("File == signUp");
			remainingPath = "signUp//signUp.json";
		}
		
		else {
			log.error("File path can't be completed Please add File path in getUserData Function");
			
			
		}
		
		
		
        JSONParser parser = new JSONParser();
        log.debug("Getting testData value from file"+ File+" and Object from JSON is "+ object+" and the value is "+val);
		Object obj = parser.parse(new FileReader(
				config.getProperty("testDataPath")+remainingPath));

		log.debug("Json Object Created for "+ config.getProperty("testDataPath")+remainingPath );
        JSONObject jsonObject = (JSONObject) obj;
        JSONObject name =  (JSONObject) jsonObject.get(object);
        String value =  (String) name.get(val);
        
		
		return value;
		
	}
	
	
	
	public String getUserData(String File , String val ) throws FileNotFoundException, IOException, ParseException {
		String remainingPath = "";
		config = new Properties();
		
		 FileInputStream configFile = new FileInputStream(System.getProperty("user.dir")+"//conf.properties");
		 config.load(configFile);


		if(File == "results") {
			log.debug("File == results");
			remainingPath = "reports//results.json";
		}
		
		else if (File == "login") {
			log.debug("File == login");
			remainingPath = "signIn//signIn.json";
		}
		
		else if (File == "signUp") {
			log.debug("File == signUp");
			remainingPath = "signUp//signUp.json";
		}
		
		else {
			log.error("File path can't be completed Please add File path in getUserData Function");
			
			
		}
		
		
		
        JSONParser parser = new JSONParser();
        log.debug("Getting testData value from file"+ File+" and the value is "+val);
		//System.out.println(config.getProperty("testDataPath"));
		//System.out.println(remainingPath);
		Object obj = parser.parse(new FileReader(
				config.getProperty("testDataPath")+remainingPath));

       // System.out.println("Object created");
		log.debug("Json Object Created for "+ config.getProperty("testDataPath")+remainingPath );		
        JSONObject jsonObject = (JSONObject) obj;
        String value =  (String) jsonObject.get(val);
        
		
		return value;
		
	}
	
	
	
	
	public String loadElement(String File , String element) throws IOException {
		elements = new Properties();		
	FileInputStream elementsValue = new FileInputStream(System.getProperty("user.dir")+"//src//main//java//"+OR+"//"+File+".properties");

	elements.load(elementsValue);

	String value = elements.getProperty(element).toString();
	log.debug("Mobile element value is "+ value +" Obtained from file "+File+" with key "+ element);
	
	return value;
	
		
		
	}
	
	
	
	
	
	
	
	
	
	///////////////////////////////////Basic Methods Used///////////////////////////////////
	
	
	 public boolean isDeviceLocked(){
		 log.info("Element clicked ");
		 log.info("Checking if device is locked");
	 		boolean status =driver.isDeviceLocked();
	 		System.out.println("device Locked"+ status);
	 		return status;
	 	}
	 	
	 	public void unlockDevice(){
			 log.info("Unlocked device");

	 		driver.unlockDevice();
	 	
	 	}
	
	 	public void clickElement(String s){
			 log.info("Clicking Element "+s);

	 		AndroidElement e =elementFinder(s);
	 		e.click();
	 		
		}
	 	
		public void softClickElement(String element){
			 log.info("Soft Clicking Element "+element);

				int i =numberOfElements(element);

				if(i>0) {
			 		AndroidElement e =elementFinder(element);
			 		e.click();	
				}
				
				
				else {
					 log.info("Soft element not found "+element+" so unable to click");
				}
		}
		
		
	 	
		public void permissionHandler(String s){
			
			driver.findElement(MobileBy.xpath(s)).click();
	 		
		}
	 	
		public void sendKeysElement(String element, String value){
			 log.info("SendKeys"+ element + " and Value is"+ value);

	 		AndroidElement e =elementFinder(element);
	 		e.sendKeys(value);

	 		
		}public void sendKeysElement(String element, CharSequence[] value){
			 log.info("SendKeys"+ element + " and Value is"+ value);

	 		AndroidElement e =elementFinder(element);
	 		e.sendKeys(value);

	 		
		}
		
		public boolean isAppFreshlyInstalled() throws IOException{	
			 log.info("Checking if app is freshly insatlled");
			int q =numberOfElements(loadElement("AppIntroduction","healthCareNeedsIntro"));
			if(q==0) 
				return false;
			else 
				return true;
		}
		
		
		
		public void assertElementPresent(String element) {
			 log.info("Asserting If element present"+ element);

			AndroidElement e =elementFinder(element);
			
			Assert.assertTrue(e.isDisplayed(), "element is not displayed"+element);

		}
		
		public void assertElementNotPresent(String element) {
			 log.info("Asserting If element present"+ element);
			 int i =numberOfElements(element);
			 if(i==0) {
				 Assert.assertTrue(true);
			 }
			 else {
				 Assert.assertTrue(false,"element still present "+element);
			 }
		}
		
		public void softAssertElementPresent(String element) {
				int i =numberOfElements(element);
				softAssert.assertTrue(i>0, "Element not found "+ element);
				if(i>0) {
					AndroidElement e =elementFinder(element);
					log.info("Asserting If element present"+ element);
					Assert.assertTrue(e.isDisplayed(), "element is not displayed"+element);
				}
				
				else {
					softAssert.fail("Elemnt Not Present "+ element);
					log.error("element not present "+ element); 
				}
		}
		
		public void softAssertTextOfElement(String element, String value) {

			
			int i =numberOfElements(element);
			
			if(i>0) {
				AndroidElement e =elementFinder(element);
				log.info("Asserting If element present"+ element);
				softAssert.assertEquals(e.getText(),value,"Expected value is------------------ " +value+" but real value is "+ e.getText() );
			}
			
			else {
				softAssert.fail("Elemnt Not Present "+ element);
				log.error("element not present "+ element+" so skinng this verification"); 
			}
		

	}
		
		
		public int numberOfElements(String s) {
			 log.info("Number of elements with element "+ s);

			String[] elements =s.split("%");
		
			
	 		int number= -1;
			if(elements[0].equals("Xpath"))
			number =driver.findElementsByXPath(elements[1]).size();
			
			else if(elements[0].equals("Id"))
				number =driver.findElementsById(elements[1]).size();
		 		
		 		else {
		 			System.out.println("Please validate or write type of Identifier before element");
		 		}
	 		
			return number;
		}
		
		
		
		
		
		public void closeAllIntroTabs() throws InterruptedException, IOException {
			 log.info("Closing all intro tabs ");

			clickElement(loadElement("AppIntroduction","healthCareNeedsIntro"));
		}
		
		
	
		
		
		
		public AndroidElement elementFinder(String s) {
			 log.info("Asserting number element present"+ s);

			AndroidElement e = null;
			
			
			String[] elements =s.split("%");
	 		
	 		if(elements[0].equals("Xpath"))
	 		e = driver.findElementByXPath(elements[1]);
	 
	 		
	 		else if(elements[0].equals("Id"))
			e = driver.findElementById(elements[1]);
	 		
	 		else if(elements[0].equals("UI"))
	 			e = driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector()).scrollIntoView(text(\""+elements[1]+"\"));");

	 		
	 		else {
	 			log.error("Please validate or write type of Identifier before element");
	 		}
			
			
			return e;	
		}
		
		public void takeScreenShotFailed(String TC) throws IOException{
			// fileName of the screenshot
			log.info("Taking screenshot for passed "+ this.getClass().getName());
			Date d=new Date();
			String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
			// store screenshot in that file
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//FailedScreenshots//"+TC+screenshotFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
			//put screenshot file in reports
			
		
			
		}
		
		public void takeScreenShotPassed(String TC) throws IOException{
			// fileName of the screenshot
			log.info("Taking screenshot for passed "+ this.getClass().getName());
			Date d=new Date();
			String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
			// store screenshot in that file
			File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir")+"//FailedScreenshots//"+TC+screenshotFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
			//put screenshot file in reports
			
		
			
		}
		
		public void longPressElement(String element) {
			log.info("Long press element "+ element);

		 t = new TouchAction(driver);
		 AndroidElement e =elementFinder(element);
		t.longPress(LongPressOptions.longPressOptions().withElement(element(e)).withDuration(Duration.ofSeconds(6))).release().perform();
		}
		
		public void dragAndDropElement(String element1 , String element2) {
			log.info("Drag and drop element "+ element1+ " and this "+ element2);

			 t = new TouchAction(driver);
			 AndroidElement e1 =elementFinder(element1);
			 AndroidElement e2 =elementFinder(element2);
			 t.longPress(element(e1)).moveTo(element(e2)).release().perform();
			}
		
		public void scrollToElementOnly(String element) {
			log.info("Scrolling to element only"+ element);

			
			AndroidElement e1 =elementFinder(element);
			
		}
		
		public void scrollToElementClick(String element) {
			log.info("Scrolling to element and then clicking on it "+ element);

			
			AndroidElement e1 =elementFinder(element);
			e1.click();
		}
		
		public void scrollToElementSendKeys(String element, String value) {
			log.info("Scrolling to element and then sendkeys on it "+ element);
			
			AndroidElement e1 =elementFinder(element);
			e1.sendKeys(value);
		}
		
		public void tapElement(String element) {
			log.info("Tapping on element "+ element);
			t = new TouchAction(driver);
			AndroidElement e1 =elementFinder(element);
			t.tap(tapOptions().withElement(element(e1)));
		}
		
		
		public void keyBoardHide() throws InterruptedException {
			Thread.sleep(2000);
			driver.hideKeyboard();
			Thread.sleep(2000);


		}
		
		
		public void checkAttributeValue(String element,String attribute, String value) {
			log.info("Checking Attribute of element "+ element);
			AndroidElement e1 =elementFinder(element);
			softAssert.assertEquals(e1.getAttribute(attribute), value,"SoftAssert failed for Attribute "+ attribute+ " expected value is "+ value +" but real value is "+ e1.getAttribute(attribute));
			
			
			
		}
		
		
		
		public String getOtpFromDBUsingEmailId(String EmailId) {
			log.info("getOtpFromDBUsingEmailId Called");
			String otp="";
			char[] password = password1.toCharArray();		
			 MongoCredential credential = MongoCredential.createCredential(userName, database, password);

			 MongoClient mongoClient1 = new MongoClient(new ServerAddress(IP, 27018),
			                                           Arrays.asList(credential)
			                                           );
			log.info("Connection Completed");

			 MongoCollection<Document> UserOTP =mongoClient1.getDatabase(database).getCollection("userOTP");
			 
			 
			 BasicDBObject inQuery = new BasicDBObject("emailId", EmailId);
			 
			 MongoCursor<Document> cursor = UserOTP.find(inQuery).iterator();

			 try {
				
			     while (cursor.hasNext()) {
						//log.info("OTP query executed and got value "+cursor.next().toJson());

			         //System.out.println(cursor.next().toJson());
			         String j = cursor.next().toJson();
			         log.info("OTP query executed and got value "+ j);
			         String[] couple = j.split(",");

			         for(int i =0; i < couple.length ; i++) {
			            String[] items =couple[i].split(":");

			            if(items[0].contains("oneTimePassword")) {
			            	otp = items[1];
							log.info("OTP value is "+items[1]);
			            }
			        }	         
			     }
			 } finally {
			     cursor.close();
			 }
			
			
			return otp;
		}
		
		
		
		public String getOtpFromDBUsingMobileNumberId(String MobileNo) {
			String otp="";
			char[] password = password1.toCharArray();		
			 MongoCredential credential = MongoCredential.createCredential(userName, database, password);

			 MongoClient mongoClient1 = new MongoClient(new ServerAddress(IP, 27018),
			                                           Arrays.asList(credential)
			                                           );
			 
			 MongoCollection<Document> UserOTP =mongoClient1.getDatabase(database).getCollection("userOTP");
			 
			 BasicDBObject inQuery = new BasicDBObject("mobileNumber", MobileNo);
			 
			 MongoCursor<Document> cursor = UserOTP.find(inQuery).iterator();
			 try {
				
			     while (cursor.hasNext()) {
			         //System.out.println(cursor.next().toJson());
			         String j = cursor.next().toJson();
	         
			         String[] couple = j.split(",");

			         for(int i =0; i < couple.length ; i++) {
			            String[] items =couple[i].split(":");

			            if(items[0].contains("oneTimePassword")) {
				         otp = items[1];
			            }
			            
			        }	         
			     }
			 } finally {
			     cursor.close();
			 }
			
			
			return otp;
		}
		
		
		
		public void LoginByUserId(String Username,String Password) throws IOException {
			log.debug("--------------In Login Function--------------");
			log.info("Login function called for USer "+Username+ " and Password "+Password );
			clickElement(loadElement("HomePage","appointments"));
					clickElement(loadElement("LoginToContinue","loginButton"));
			
			sendKeysElement(loadElement("LoginPage","inputUserId"),Username);

			sendKeysElement(loadElement("LoginPage","password"),Password);
			clickElement(loadElement("LoginPage","loginButton"));		
			clickElement(loadElement("LoginPage","permissionForCalls"));
			
			clickElement(loadElement("SetQuickAccessKey","setLater"));
			assertElementPresent(loadElement("HomePage","appointments"));
			
			
		}
		
		
		
		public int RandomNumber() {
			random = (int) (Math.random()*9999);
			log.info("Random Number is "+ random);
			return random;
		}
		
		
//		public  void minimizeMaximize() {
//			try {
//			driver.runAppInBackground(Duration.ofSeconds(6));
//			((AndroidDriver) driver).startActivity("appPackage", "appActivity");
//			} catch (Exception e) {
//			e.printStackTrace();
//			}
//			}
		/*public void backButton() throws InterruptedException {
			Thread.sleep(10000);
		driver.pressKeyCode(AndroidKeyCode.BACK);
		}*/
		
		
//		public  void minimizeMaximize() {
//			try {
//			driver.runAppInBackground(Duration.ofSeconds(6));
//			((AndroidDriver) driver).startActivity("com.mphrx.patient","com.mphrx.");
//			} catch (Exception e) {
//			e.printStackTrace();
//			}
//			}
		

		



		
}
